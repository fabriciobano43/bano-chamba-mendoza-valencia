/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.model;

/**
 *
 * @author Javier
 */
public class Editor {

    private String texto;
    private static String aux;

    public static String getAux() {
        return aux;
    }

    public static void setAux(String aux) {
        Editor.aux = aux;
    }

    public String getTexto(){
        if (getAux() != null) {
            texto = getAux();
        }
        return texto;
    }

    public void setTexto(String texto) {
        this.setAux(texto);
        this.texto = texto;
    }

}
