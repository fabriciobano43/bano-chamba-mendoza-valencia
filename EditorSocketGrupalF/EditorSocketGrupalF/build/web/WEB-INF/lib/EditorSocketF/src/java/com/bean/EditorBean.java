/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bean;

import com.model.Editor;
import javax.faces.application.FacesMessage;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.ManagedBean;
import org.apache.commons.lang3.StringEscapeUtils;
import org.primefaces.push.EventBus;
import org.primefaces.push.EventBusFactory;

/**
 *
 * @author Javier
 */


@ManagedBean
@RequestScoped
public class EditorBean {
     
    private Editor editor =new Editor();
    private static String text="";

    public Editor getEditor() {
        return editor;
    }

    public void setEditor(Editor editor) {
        this.editor = editor;
    }
    
    public String getText() {
        return text;
    }
 
    public void setText(String text) {
        this.text = text;
    }
    
     public void agregar(){
        //this.text=editor.getTexto();
        notificarPUSH();
    }
    
     public void notificarPUSH(){
        String summary="Nuevo elemento";
        String detail="Se agrego a la lista";
        String CHANNEL="/dato";
        
        EventBus eventBus=EventBusFactory.getDefault().eventBus();
        eventBus.publish(CHANNEL,new FacesMessage(StringEscapeUtils.escapeHtml3(summary),StringEscapeUtils.escapeHtml3(detail)));
    }
    
}
