/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bean;

import com.model.Agregar;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import org.primefaces.context.RequestContext;

/**
 *
 * @author Javier
 */
@ManagedBean
@RequestScoped
public class AccesoBean {

    private String user;
    private String pass;

    private Agregar agregar = new Agregar();
    private static List<Agregar> lista = new ArrayList();

    public AccesoBean() {
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public Agregar getAgregar() {
        return agregar;
    }

    public void setAgregar(Agregar agregar) {
        this.agregar = agregar;
    }

    public List<Agregar> getLista() {
        return lista;
    }

    public void setLista(List<Agregar> lista) {
        AccesoBean.lista = lista;
    }
    
    // En este metodo se especifica el usuario y clave del adminitrador o persona que va a controlar 
    // el documento y no se podra acceder al documento hasta que esa persona ingrese ya sea como el creador del documento 
    // o como un usuario que tengo permisos para accedor al documento
    public String acceder() throws IOException {
        if (user.equals("admin") && pass.equals("admin")) {
            //String path = FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath();
            //path=/Dialog
            RequestContext.getCurrentInstance().execute("PF('login').hide();formboton.style.display = 'block' ;");
            //FacesContext.getCurrentInstance().getExternalContext().redirect(path+"/faces/index.xhtml");
        } else if (buscar()){
            RequestContext.getCurrentInstance().execute("PF('login').hide();");
        }else{
            RequestContext.getCurrentInstance().execute("PF('login').show();");
            FacesMessage msg = new FacesMessage("Usuario inválido");
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
        return "editor";
    }
    
    //Este metodo se lo utiliza para registrar nuevos usuarios al documento
    public void add(){
        this.lista.add(agregar);
        for (int i = 0; i < lista.size(); i++) {
            System.out.println(lista.get(i).getUsuario()+" "+lista.get(i).getPassword()+"\n");
        }
    }
    
    // Este metodo se lo utiliza para buscar si un usuario que desea ingresar al documento existe
    public boolean buscar(){
        for (int i = 0; i < lista.size(); i++) {
            if (user.equals(lista.get(i).getUsuario()) && pass.equals(lista.get(i).getPassword())) {
                return true;
            }
        }
        return false;
    }

}
