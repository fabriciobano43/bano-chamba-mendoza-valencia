/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.socket;

import javax.faces.application.FacesMessage;
import org.primefaces.push.annotation.OnMessage;
import org.primefaces.push.annotation.PushEndpoint;
import org.primefaces.push.impl.JSONEncoder;

/**
 *
 * @author Javier
 */
@PushEndpoint("/dato")

//Aqui es la clase Endpoint que nos ayuda al envio de informacion del socket para poder interactuar en tiempo
// real en el documento con todos los usuarios que esten escribiendo en el

public class DatoResource {
    
    @OnMessage(encoders = {JSONEncoder.class})
    public  FacesMessage onMessage(FacesMessage message){
        return message;
    }
    
}
