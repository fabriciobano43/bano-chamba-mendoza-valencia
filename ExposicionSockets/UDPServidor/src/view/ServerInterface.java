/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import sun.net.www.content.audio.x_aiff;

/**
 *
 * @author Javier
 */
     
public final class ServerInterface extends JFrame implements Runnable{

    private final JLabel lblTitle, lblMessages;
    private final JTextArea txtMessages;
    private final JPanel panel;
    private final Thread thread;

    public ServerInterface(String title) {
        lblTitle = new JLabel("Servidor");
        lblMessages = new JLabel("Chat: ");
        txtMessages = new JTextArea(10, 25);
        panel = new JPanel();

        panel.add(lblTitle);
        panel.add(lblMessages);
        panel.add(txtMessages);

        this.setBounds(800, 350, 320, 410);
        this.add(panel);
        this.setVisible(true);
        this.setTitle(title);

        thread=new Thread(this);
        thread.start();
    }

    @Override
    public void run() {
        DatagramSocket ioConexion;
        byte datos[]=new byte[1024];
        System.out.println("Esperando un datagrama");
        try {
            ioConexion=new DatagramSocket(9999);
            while (true) {                
                try {
                    DatagramPacket r = new DatagramPacket(datos, datos.length);
                    ioConexion.receive(r);
                    txtMessages.append(new String(datos)+"\n");
                } catch (Exception ex) {
                    System.out.println(ex.getMessage());
                }
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
