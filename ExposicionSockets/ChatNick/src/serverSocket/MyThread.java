/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serverSocket;

import client.Message;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;
import views.ServerInterface;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author gugle
 */
public class MyThread extends Thread
{
    private ServerInterface serverInterface;
    
    public MyThread(String name, ServerInterface serverInterfaz){
        super(name);
        this.serverInterface = serverInterfaz;
        start();
        
    }

    @Override
    public void run() {
        ServerSocket serverSocket;
        List<String> usuarios= new ArrayList();
        List<String> ips= new ArrayList();
        Socket server;
        try {
            serverSocket = new ServerSocket(9999);
            String ip;
            Message receivedMessage;
            while (true)
            {
                try{
                    server = serverSocket.accept();
                    System.out.println (server.getLocalAddress().getHostAddress());
                    //trae la informacion
                    ObjectInputStream objectInputStream = new ObjectInputStream(server.getInputStream());
                    try {
                        
                        String nick;
                        receivedMessage = (Message) objectInputStream.readObject();
                        ip=receivedMessage.getIp();
                        nick=receivedMessage.getNick();
                        if(!ip.equals("online"))
                        {
                            serverInterface.getMenssages().append("\nNick: " + receivedMessage.getNick() + " - Ip: " 
                                + receivedMessage.getIp() + " - Texto: " + receivedMessage.getTexto());
                            //reenviar paquete al destinatario
                         Socket Destinatario= new Socket(ip,9090);
                         ObjectOutputStream reenvio=new ObjectOutputStream(Destinatario.getOutputStream());
                         reenvio.writeObject(receivedMessage);
                         Destinatario.close();
                        }
                        //Cuando recien inicia sesión
                        else{
                            InetAddress direccionip = server.getInetAddress();
                            String clienteip=direccionip.getHostAddress();
                            serverInterface.getMenssages().append("\nSe conecto " + nick +" ip: "+ clienteip);
                            usuarios.add(nick);
                            ips.add(clienteip);
                            receivedMessage.setUsuarios(usuarios);
                            receivedMessage.setIps(ips);
                            //enviando ips a todos los clientes
                            for (String i:ips){
                             Socket Destinatario= new Socket(i,9090);
                             ObjectOutputStream reenvio=new ObjectOutputStream(Destinatario.getOutputStream());
                             reenvio.writeObject(receivedMessage);
                             Destinatario.close();
                            }
                        }
                         
                    } catch (ClassNotFoundException ex) {
                        Logger.getLogger(MyThread.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    //serverInterface.getMenssages().setText(serverInterface.getMenssages().getText() + "\n" + inputStream.readUTF());
                    server.close();
                }
                catch (IOException ex){
                    System.out.println (ex.getMessage());
                }
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }    
}
