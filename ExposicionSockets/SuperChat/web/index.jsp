<%-- 
    Document   : index
    Created on : 11-ago-2020, 17:57:34
    Author     : JoseRene
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Chateando Con Amigos</title>
    </head>
    <body>
         <h2>WebSockets : Chateando</h2>        
        <form action="">
		<input id="usuario" name="user" value="" type="text"> 
		<input id="btnusuario" onclick="entrar();" value="Entrar" type="button">
		
		<br/><br/>
		<textarea id="pantalla" rows="10" cols="30" readonly disabled></textarea>
		
		<br/>
		<input id="mensaje" name="message" value="" type="text" disabled>
		<input id="btnenviar" onclick="enviarmensaje();" value="Enviar" type="button" disabled>
	</form>
         
         <script type="text/javascript" src="websocket.js"></script>
               
    </body>
</html>
