/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

/**
 *
 * @author Javier
 */
public final class ClientInterface extends JFrame {

    private final JLabel lblTitle, lblClient, lblMessage, lblMessages;
    private final JTextField txtMessage, txtClient;
    private final JTextArea txtMessages;
    private final JButton btnEnviar;
    private final JPanel panel;

    
    DatagramSocket ioConexion;
    byte datos[]=new byte[1024];

    public ClientInterface(String title) {
        lblTitle = new JLabel("Chat (Cliente)");
        lblClient = new JLabel("Nick: ");
        lblMessage = new JLabel("Mensaje: ");
        lblMessages = new JLabel("Chat: ");
        txtMessage = new JTextField(25);
        txtMessages = new JTextArea(10, 25);
        txtClient = new JTextField(25);
        btnEnviar = new JButton("Enviar");
        panel = new JPanel();

        panel.add(lblTitle);
        panel.add(lblClient);
        panel.add(txtClient);
        panel.add(lblMessages);
        panel.add(txtMessages);
        panel.add(lblMessage);
        panel.add(txtMessage);

        btnEnviar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                
                try{
                    datos=(txtClient.getText()+": "+txtMessage.getText()).getBytes();
                    ioConexion=new DatagramSocket();
                    
                    DatagramPacket enviar=new DatagramPacket(datos, datos.length, InetAddress.getByName("192.168.1.9"), 9999);
                    ioConexion.send(enviar);
                    txtMessages.append(txtClient.getText()+": "+txtMessage.getText()+"\n");
                    txtMessage.setText("");
                    ioConexion.close();
                }catch(Exception ex){
                    System.out.println(ex.getMessage());
                }
              
            }
        });

        panel.add(btnEnviar);

        this.setBounds(800, 500, 320, 410);
        this.add(panel);
        this.setVisible(true);
        this.setTitle(title);
    }

}
