/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cliente;

import java.io.*;
import java.net.*;
import java.util.Scanner;

/**
 *
 * @author Javier
 */
public class Cliente {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        DatagramSocket ioConexion;
        byte datos[] = new byte[30];
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingrese texto a enviar: ");
        String cadena = sc.nextLine();
        datos = cadena.getBytes(); // Convierte de String a byte
        try {
            ioConexion = new DatagramSocket();
// Crea paquete para el envío de datos de tipo "byte", el largo de los
// datos y la dirección IP y el puerto del servidor
            DatagramPacket enviar = new DatagramPacket(datos, datos.length,
                    InetAddress.getByName("192.168.1.9"), 9999);
            ioConexion.send(enviar); // envía paquete
            ioConexion.close();
        } catch (IOException e) {
            System.out.println(e);
        }
    }

}
