/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package views;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.DataInputStream;
import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.Socket;
import javax.net.ssl.SSLSocket;
import javax.swing.*;

/**
 *
 * @author Javier
 */
public final class ClientInterface extends JFrame {

    private final JLabel lblTitle, lblImagen, lblIMG, lblTXT;
    private final JTextField txtClient;
    private final JTextArea txtArchivo;
    private final JButton btnEnviar, btnSubir;
    private final JPanel panel, panelBoton;
    private final JScrollPane panelIMG;
    private final JScrollPane panelTXT;
    private final JFileChooser seleccionado = new JFileChooser();

    File archivo;
    FileInputStream entradaImg, entradaTxt;

    DatagramSocket ioConexion;
    byte[] img_txt;// = new byte[Byte.MAX_VALUE];
    String rutaImagen="", rutasImagenes="";
    String rutaTxts="", rutaTxt="";

    public ClientInterface(String title) {
        lblTitle = new JLabel("Cliente:");
        lblImagen = new JLabel();
        lblImagen.setPreferredSize(new Dimension(1, 500));
        txtClient = new JTextField(25);
        txtArchivo = new JTextArea();
        lblIMG = new JLabel("Imagen: ");
        lblIMG.setPreferredSize(new Dimension(1, 20));
        lblTXT = new JLabel("Archivo de texto: ");
        btnSubir = new JButton("Subir");
        btnEnviar = new JButton("Enviar");
        panel = new JPanel(new GridLayout(0, 2));
        panelBoton = new JPanel(new GridLayout(0, 2));

        panelIMG = new JScrollPane();
        panelTXT = new JScrollPane();

        panel.add(lblTitle);
        panel.add(txtClient);
        panel.add(lblIMG);
        panel.add(lblTXT);
        panelIMG.setViewportView(lblImagen);
        panelTXT.setViewportView(txtArchivo);

        btnSubir.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    if (seleccionado.showDialog(null, "SELECCIONE EL ARCHIVO") == JFileChooser.APPROVE_OPTION) {
                        archivo = seleccionado.getSelectedFile();
                        if (archivo.canRead()) {
                            if (archivo.getName().endsWith("png") || archivo.getName().endsWith("jpg") || archivo.getName().endsWith("JPG")) {
                                entradaImg = new FileInputStream(archivo);
                                img_txt = new byte[(int) archivo.length()];
                                entradaImg.read(img_txt);
                                rutaImagen=archivo.getAbsolutePath()+"*";
                                rutasImagenes+=rutaImagen;
                                img_txt = new byte[rutasImagenes.length()];
                                img_txt=rutasImagenes.getBytes();
                                System.out.println(archivo.getAbsolutePath());
                                //lblImagen.setIcon(new ImageIcon(img_txt));
                                ImageIcon imagen1 = new ImageIcon(archivo.getPath());
                                Icon fondo = new ImageIcon(imagen1.getImage().getScaledInstance(lblImagen.getWidth(), lblImagen.getHeight(), Image.SCALE_SMOOTH));
                                lblImagen.setIcon(fondo);
                                entradaImg.close();
                            } else if (archivo.getName().endsWith("txt")) {
                                String contenido="";
                                entradaTxt = new FileInputStream(archivo);
                                int ascci;
                                while ((ascci = entradaTxt.read()) != -1) {
                                    char caracter = (char) ascci;
                                    contenido += caracter;
                                }
                                rutaTxt="@"+archivo.getAbsolutePath()+"*";
                                rutaTxts+=rutaTxt;
                                img_txt = new byte[rutaTxts.length()];
                                img_txt=rutaTxts.getBytes();
                                txtArchivo.setText(contenido);
                                entradaTxt.close();
                            }else {
                                JOptionPane.showMessageDialog(null, "Seleccione un archivo con formato de texto o imagen");
                            }
                        }
                    }
                } catch (Exception ex) {
                    System.out.println(ex.getMessage());
                }

            }
        });

        btnEnviar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                try {
                    ioConexion = new DatagramSocket();
                    // Crea paquete para el envío de img_txt de tipo "byte", el largo de los
                    // img_txt y la dirección IP y el puerto del servidor
                    DatagramPacket enviar = new DatagramPacket(img_txt,img_txt.length, InetAddress.getByName("192.168.1.9"), 9999);
                    ioConexion.send(enviar); // envía paquete
                    ioConexion.close();

                } catch (Exception ex) {
                    System.out.println(ex.getMessage());
                }

            }
        });
        btnSubir.setPreferredSize(new Dimension(50, 50));
        btnEnviar.setPreferredSize(new Dimension(50, 50));

        panelBoton.add(btnSubir);
        panelBoton.add(btnEnviar);

        JPanel panelHeader = new JPanel();
        panelHeader.setLayout(new BoxLayout(panelHeader, BoxLayout.PAGE_AXIS));
        panelHeader.add(panel);

        JPanel panelBody = new JPanel();
        panelBody.setLayout(new BoxLayout(panelBody, BoxLayout.LINE_AXIS));
        panelBody.add(panelIMG);
        panelBody.add(panelTXT);

        JPanel panelFooter = new JPanel();
        panelFooter.setLayout(new BoxLayout(panelFooter, BoxLayout.LINE_AXIS));
        panelFooter.add(panelBoton);

        JPanel mainPanel = new JPanel();
        mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.PAGE_AXIS));
        mainPanel.add(panelHeader);
        mainPanel.add(panelBody);
        mainPanel.add(panelFooter);

        this.setBounds(600, 250, 1200, 700);
        //this.add(panel);
        this.add(mainPanel);
        this.setVisible(true);
        this.setTitle(title);
        //this.pack();

    }

}
