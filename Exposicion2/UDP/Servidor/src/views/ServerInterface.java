/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package views;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import java.io.IOException;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.Serializable;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;

/**
 *
 * @author Javier
 */
public final class ServerInterface extends JFrame implements Serializable, Runnable {

    private final JLabel lblTitle, lblImagen, lblIMG, lblTXT;
    private final JPanel panel, panelBoton;
    private final Thread thread;
    private final JScrollPane panelIMG;
    private final JScrollPane panelTXT;
    private final JTextArea txtArchivo;
    private final JButton btnSgtImg, btnAtrImg, btnSgtTxt, btnAtrTxt;

    File archivoImg, archivoTxt;
    FileInputStream entradaImg, entradaTxt;
    ImageIcon imagen[];
    String imagenes[], txts[], txt[];
    Icon icono[];
    int contadorImg = 0, contadorTxt = 0, ascci;
    String cadena = "", rutaImg = "", rutaTxt = "";

    public ServerInterface(String title) {
        lblTitle = new JLabel("Chat (Servidor)");
        lblImagen = new JLabel();
        lblImagen.setPreferredSize(new Dimension(1, 400));
        txtArchivo = new JTextArea();
        lblIMG = new JLabel("Imagen: ");
        lblTXT = new JLabel("Archivo de texto: ");
        panel = new JPanel(new GridLayout(0, 2));
        btnAtrImg = new JButton("Atrás imagen");
        btnSgtImg = new JButton("Siguiente imagen");
        btnAtrTxt = new JButton("Atrás archivo de texto");
        btnSgtTxt = new JButton("Siguiente archivo de texto");
        panelBoton = new JPanel(new GridLayout(0, 4));
        panelIMG = new JScrollPane();
        panelTXT = new JScrollPane();

        panel.add(lblIMG);
        panel.add(lblTXT);
        panelIMG.setViewportView(lblImagen);
        panelTXT.setViewportView(txtArchivo);

        btnSgtImg.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                try {
                    if (contadorImg == imagenes.length - 1) {
                        contadorImg = -1;
                    }
                    contadorImg++;
                    lblImagen.setIcon(icono[contadorImg]);
                } catch (Exception ex) {
                    System.out.println(ex.getMessage());
                }

            }
        });

        btnAtrImg.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                try {
                    if (contadorImg == 0) {
                        contadorImg = imagenes.length;
                    }
                    contadorImg--;
                    lblImagen.setIcon(icono[contadorImg]);

                } catch (Exception ex) {
                    System.out.println(ex.getMessage());
                }

            }
        });

        btnSgtTxt.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                try {
                    if (contadorTxt == txts.length - 1) {
                        contadorTxt = -1;
                    }
                    contadorTxt++;
                    txtArchivo.setText("");
                    txtArchivo.setText(txt[contadorTxt]);
                } catch (Exception ex) {
                    System.out.println(ex.getMessage());
                }

            }
        });

        btnAtrTxt.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                try {
                    if (contadorTxt == 0) {
                        contadorTxt = txts.length;
                    }
                    contadorTxt--;
                    txtArchivo.setText("");
                    txtArchivo.setText(txt[contadorTxt]);

                } catch (Exception ex) {
                    System.out.println(ex.getMessage());
                }

            }
        });

        btnAtrImg.setPreferredSize(new Dimension(50, 50));
        btnSgtImg.setPreferredSize(new Dimension(50, 50));
        btnAtrTxt.setPreferredSize(new Dimension(50, 50));
        btnSgtTxt.setPreferredSize(new Dimension(50, 50));

        panelBoton.add(btnAtrImg);
        panelBoton.add(btnSgtImg);
        panelBoton.add(btnAtrTxt);
        panelBoton.add(btnSgtTxt);

        JPanel panelHeader = new JPanel();
        panelHeader.setLayout(new BoxLayout(panelHeader, BoxLayout.PAGE_AXIS));
        panelHeader.add(panel);

        JPanel panelBody = new JPanel();
        panelBody.setLayout(new BoxLayout(panelBody, BoxLayout.LINE_AXIS));
        panelBody.add(panelIMG);
        panelBody.add(panelTXT);

        JPanel panelFooter = new JPanel();
        panelFooter.setLayout(new BoxLayout(panelFooter, BoxLayout.LINE_AXIS));
        panelFooter.add(panelBoton);

        JPanel mainPanel = new JPanel();
        mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.PAGE_AXIS));
        mainPanel.add(panelHeader);
        mainPanel.add(panelBody);
        mainPanel.add(panelFooter);

        this.setBounds(600, 250, 1200, 600);
        //this.add(panel);
        this.add(mainPanel);
        this.setVisible(true);
        this.setTitle(title);
        //this.pack();

        thread = new Thread(this);
        thread.start();

    }

    @Override
    public void run() {
        DatagramSocket ioConexion;
        byte datos[] = new byte[64000];
        System.out.println("Esperando un datagrama...");
        try {
            ioConexion = new DatagramSocket(9999); // Especifica el puerto del servidor
            while (true) {
                try {
                    // Crea paquete para recibir datos de tipo "byte", especificando su largo.
                    DatagramPacket r = new DatagramPacket(datos, datos.length);
                    ioConexion.receive(r); // Espera a recibir paquete de datos.
                    cadena = new String(r.getData()); // Convierte de byte a String.
                    if (cadena.startsWith("@")) {
                        for (byte dato : datos) {
                            if (dato != 0) {
                                char caracter = (char) dato;
                                rutaTxt += caracter;
                            }
                        }
                        String[] partsTxt = rutaTxt.split("\\*");
                        txt = new String[partsTxt.length];
                        txts = new String[partsTxt.length];
                        for (int i = 0; i < partsTxt.length; i++) {
                            txts[i] = partsTxt[i].substring(1);
                            archivoTxt = new File(txts[i]);
                            entradaTxt = new FileInputStream(archivoTxt);
                            String contenidoTxt = "";
                            while ((ascci = entradaTxt.read()) != -1) {
                                char caracter = (char) ascci;
                                contenidoTxt += caracter;
                            }
                            txt[i] = contenidoTxt;
                        }
                        txtArchivo.setText(txt[0]);
                    } else {
                        for (byte dato : datos) {
                            if (dato != 0) {
                                char caracter = (char) dato;
                                rutaImg += caracter;
                            }
                        }
                        String[] partsImg = rutaImg.split("\\*");
                        imagen = new ImageIcon[partsImg.length];
                        imagenes = new String[partsImg.length];
                        icono = new Icon[partsImg.length];
                        for (int i = 0; i < partsImg.length; i++) {
                            imagenes[i] = partsImg[i];
                            archivoImg = new File(imagenes[i]);
                            entradaImg = new FileInputStream(archivoImg);
                            entradaImg.read(imagenes[i].getBytes());
                            imagen[i] = new ImageIcon(archivoImg.getPath());
                            icono[i] = new ImageIcon(imagen[i].getImage().getScaledInstance(lblImagen.getWidth(), lblImagen.getHeight(), Image.SCALE_SMOOTH));
                            entradaImg.close();
                        }
                        lblImagen.setIcon(icono[0]);

//                        rutaImg=parts[0];
//                        archivoImg = new File(rutaImg);
//                        entradaImg = new FileInputStream(archivoImg);
//                        entradaImg.read(rutaImg.getBytes());
//                        ImageIcon imagen1 = new ImageIcon(archivoImg.getPath());
//                        Icon fondo = new ImageIcon(imagen1.getImage().getScaledInstance(lblImagen.getWidth(), lblImagen.getHeight(), Image.SCALE_SMOOTH));
//                        lblImagen.setIcon(fondo);
//                        entradaImg.close();
                    }
                    //System.out.println(cadena);
                    //System.out.println(cadena);

                } catch (IOException e) {
                    System.out.println(e.getMessage());
                }
            }
        } catch (SocketException e) {
            System.out.println(e.getMessage());
        }
    }
}
