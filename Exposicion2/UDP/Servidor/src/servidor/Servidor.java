/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servidor;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;

/**
 *
 * @author Javier
 */
public class Servidor {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        DatagramSocket ioConexion;
        byte datos[] = new byte[30];
        System.out.println("Esperando un datagrama...");
        try {
            ioConexion = new DatagramSocket(9999); // Especifica el puerto del servidor
// Crea paquete para recibir datos de tipo "byte", especificando su largo.
            DatagramPacket r = new DatagramPacket(datos, datos.length);
            ioConexion.receive(r); // Espera a recibir paquete de datos.
            String cadena = new String(datos); // Convierte de byte a String.
            System.out.println(cadena);
            ioConexion.close();
        } catch (IOException e) {
            System.out.println(e);
        }
    }

}
