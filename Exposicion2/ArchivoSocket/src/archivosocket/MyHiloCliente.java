/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package archivosocket;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import javax.swing.JComboBox;
import javax.swing.JList;
import javax.swing.JTextArea;

/**
 *
 * @author USUARIO
 */
public class MyHiloCliente extends Thread {
    Clientes cliente;
   JComboBox users;
   JTextArea txtmensaje;
   JList<String> lista;
   ArrayList array = new ArrayList();
   DefaultListModel modelo = new DefaultListModel();
    public MyHiloCliente(Clientes cli,JComboBox users,JTextArea txtmensaje,JList<String> lista) {
        
        this.cliente=cli;
        this.users=users;
        this.txtmensaje=txtmensaje;
        this.lista=lista;
    
    }
    
     public void run()
   {
      //Recibir mensajes de otros usuarios
        try {
            
            ServerSocket servidor=new ServerSocket(9090);
            Socket cliente;
            Message messagerecived;
             while(true){ 
                 //socket pasivo
                cliente=servidor.accept();
                 ObjectInputStream entradadatos= new ObjectInputStream(cliente.getInputStream());
                 messagerecived=(Message) entradadatos.readObject();
                 if(!(messagerecived.getIp().equals("online"))){
                   FileOutputStream file=new FileOutputStream("C:/archivos_chat/"+messagerecived.getNombre_archivo());
                   file.write(messagerecived.getBut());
                   txtmensaje.append("\n"+messagerecived.getNick()+" envio el archivo: "+messagerecived.getNombre_archivo());
                      //corregir
                   if(!array.contains(messagerecived.getNombre_archivo())){
                       array.add(messagerecived.getNombre_archivo());
                       modelo.addElement(messagerecived.getNombre_archivo());
                       lista.setModel(modelo);
                   }
                 }
                 //Recibe las ip y los nick cada vez que un usuario se conecta
                 else
                 {
                     
                    List<String> nicks=new ArrayList();
                    this.cliente.ips=messagerecived.getIps();
                    nicks=messagerecived.getUsuarios();
                    users.removeAllItems();
                    for(String z:nicks){
                        users.addItem(z);
                    }
                 }
            }
        } catch (IOException ex) {
            Logger.getLogger(Clientes.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Clientes.class.getName()).log(Level.SEVERE, null, ex);
        }
   }
}
