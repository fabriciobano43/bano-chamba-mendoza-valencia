/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package archivosocket;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author gugle
 */
public class Message implements Serializable{
    private String nick, ip, texto;
    List<String> usuarios= new ArrayList();
    List<String> ips= new ArrayList();
    byte[] but;
    String nombre_archivo;

    public String getNombre_archivo() {
        return nombre_archivo;
    }

    public void setNombre_archivo(String nombre_archivo) {
        this.nombre_archivo = nombre_archivo;
    }
     
    public byte[] getBut() {
        return but;
    }

    public void setBut(byte[] but) {
        this.but = but;
    }
   

    public List<String> getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(List<String> usuarios) {
        this.usuarios = usuarios;
    }

    public List<String> getIps() {
        return ips;
    }

    public void setIps(List<String> ips) {
        this.ips = ips;
    }

    public Message(String nick, String ip, String texto, String nombre_archivo) {
        this.nick = nick;
        this.ip = ip;
        this.texto = texto;
        this.nombre_archivo=nombre_archivo;
    }

    public Message() {
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }
}
