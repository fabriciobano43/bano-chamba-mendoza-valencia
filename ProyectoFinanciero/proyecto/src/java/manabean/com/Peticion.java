/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package manabean.com;

import com.modelo.Clientes;
import com.modelo.Historal;
import com.modelo.Pagar;
import com.sun.java.swing.plaf.windows.resources.windows;
import java.io.IOException;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletResponse;

@ManagedBean
@ViewScoped
@Named
public class Peticion {

    public List<Pagar> getListap() {
        return listap;
    }

    public void setListap(List<Pagar> listap) {
        this.listap = listap;
    }

    private List<Clientes> lista = new ArrayList();
    private List<Pagar> listapp = new ArrayList();

    public List<Pagar> getListapp() {
        return listapp;
    }

    public void setListapp(List<Pagar> listapp) {
        this.listapp = listapp;
    }
    private List<Pagar> listap = new ArrayList();

    private List<Historal> listahisto = new ArrayList();

    public List<Historal> getListahisto() {
        return listahisto;
    }

    public void setListahisto(List<Historal> listahisto) {
        this.listahisto = listahisto;
    }

    private static int id;

    private static int codigoCred;

    public static int getCodigoCred() {
        return codigoCred;
    }

    public static void setCodigoCred(int codigoCred) {
        Peticion.codigoCred = codigoCred;
    }

    public static int getId() {
        return id;
    }

    public static void setId(int id) {
        Peticion.id = id;
    }

    private double prestamo = 0;

    private String disable;

    public String getDisable() {
        return disable;
    }

    public void setDisable(String disable) {
        this.disable = disable;
    }

    public double getPrestamo() {
        return prestamo;
    }

    public void setPrestamo(double prestamo) {
        this.prestamo = prestamo;
    }

    private Clientes pagarcredito;

    private Pagar pagatodo;

    public Pagar getPagatodo() {
        return pagatodo;
    }

    public void setPagatodo(Pagar pagatodo) {
        this.pagatodo = pagatodo;

        int an = 0, me = 0, di = 0;
        try {
            Conexion con = new Conexion();
            List<Pagar> listacreditos = new ArrayList();
            Calendar fecha = new GregorianCalendar();
            int ani = fecha.get(Calendar.YEAR);
            int mes = fecha.get(Calendar.MONTH) + 1;
            int dia = fecha.get(Calendar.DAY_OF_MONTH);

            String registrfecha = String.valueOf(ani + "-" + mes + "-" + dia);

            String fechapago = con.obtenerfechapago(pagatodo.getId());
            String obt = "";
            int aux = 0;
            for (int i = 0; i < fechapago.length(); i++) {
                if (fechapago.charAt(i) == '-' && aux == 0) {
                    an = Integer.parseInt(obt);
                    aux++;
                    obt = "";
                    i++;
                } else if (fechapago.charAt(i) == '-' && aux == 1) {
                    me = Integer.parseInt(obt);
                    aux++;
                    obt = "";
                    i++;
                } else if ((i + 1) == fechapago.length() && aux == 2) {
                    obt = obt + String.valueOf(fechapago.charAt(i));
                    di = Integer.parseInt(obt);
                    aux++;
                    obt = "";
                }
                obt = obt + String.valueOf(fechapago.charAt(i));
            }

            int ux = 0;
            int au = 1;

            if (an > ani && me > mes) {
                //  mes = me;
                //  dia = di;
                ux = 1;
            }

            if (ani == an && mes == me && dia > di) {
                // dia = di;
                au = 0;
            }

            if (ani <= an && mes <= me && dia <= di && ux == 1 && au == 1) {
                con = new Conexion();
                con.actualizarpago(registrfecha, pagatodo.getId());

                listap = con.obtenerpagos(codigoCred);

                con = new Conexion();
                listacreditos = con.obtenerpagos(codigoCred);
                if (listacreditos.size() == 0) {
                  //  con = new Conexion();

//                    String fechalimite = con.fechalimite(codigoCred);
//
//                    obt = "";
//                    aux = 0;
//                    for (int i = 0; i < fechalimite.length(); i++) {
//                        if (fechalimite.charAt(i) == '-' && aux == 0) {
//                            an = Integer.parseInt(obt);
//                            aux++;
//                            obt = "";
//                            i++;
//                        } else if (fechalimite.charAt(i) == '-' && aux == 1) {
//                            me = Integer.parseInt(obt);
//                            aux++;
//                            obt = "";
//                            i++;
//                        } else if ((i + 1) == fechalimite.length() && aux == 2) {
//                            obt = obt + String.valueOf(fechalimite.charAt(i));
//                            di = Integer.parseInt(obt);
//                            aux++;
//                            obt = "";
//                        }
//                        obt = obt + String.valueOf(fechalimite.charAt(i));
//                    }

                   // int ux1 = 0;
                   // int au1 = 1;

                  //  if (an > ani && me > mes) {
                        //  mes = me;
                        //  dia = di;
                    //    ux1 = 1;
                   // }

                  //  if (ani == an && mes == me && dia > di) {
                        // dia = di;
                  //      au1 = 0;
                  //  }
                    
                  //  if (ani <= an && mes <= me && dia <= di && ux1 == 1 && au1 == 1) {
                        con = new Conexion();
                        con.actualizarcredito(codigoCred, registrfecha);
                   // } 
                    //else {
                    //    con = new Conexion();
                    //    String fechafinal = String.valueOf(dia + "-" + mes + "-" + ani);
                    //    con.actualizarcredito(codigoCred, fechafinal);
                   // }
                }

            } else {
                con = new Conexion();
                String fechafinal = String.valueOf(dia + "-" + mes + "-" + ani);
                con.actualizarpago(fechafinal, pagatodo.getId());
                con = new Conexion();
                listacreditos = con.obtenerpagos(codigoCred);
                if (listacreditos.size() == 0) {
                    con = new Conexion();
                    String fechalimite = con.fechalimite(codigoCred);
                    obt = "";
                    aux = 0;
                    for (int i = 0; i < fechalimite.length(); i++) {
                        if (fechalimite.charAt(i) == '-' && aux == 0) {
                            an = Integer.parseInt(obt);
                            aux++;
                            obt = "";
                            i++;
                        } else if (fechalimite.charAt(i) == '-' && aux == 1) {
                            me = Integer.parseInt(obt);
                            aux++;
                            obt = "";
                            i++;
                        } else if ((i + 1) == fechalimite.length() && aux == 2) {
                            obt = obt + String.valueOf(fechalimite.charAt(i));
                            di = Integer.parseInt(obt);
                            aux++;
                            obt = "";
                        }
                        obt = obt + String.valueOf(fechalimite.charAt(i));
                    }

                    if (an > ani && mes > me) {
                        mes = me;
                        dia = di;
                    }

                    if (ani <= an && mes <= me && dia > di) {
                        dia = di;
                    }
                    if (ani <= an && mes <= me && dia <= di) {
                        con = new Conexion();
                        con.actualizarcredito(codigoCred, fechalimite);
                    } else {
                        con = new Conexion();
                        String fechafinalin = String.valueOf(dia + "-" + mes + "-" + ani);
                        con.actualizarcredito(codigoCred, fechafinalin);
                    }
                }
            }

        } catch (Exception e) {
            e.getMessage();
        }
    }

    public Clientes getPagarcredito() {
        return pagarcredito;
    }

    private Pagar clientote;

    public Pagar getClientote() {
        return clientote;
    }

    public void setClientote(Pagar clientote) {
        this.clientote = clientote;
        Conexion con = new Conexion();
        List<Integer> idescredito = new ArrayList();
        idescredito = con.idescredito(clientote.getId());

        int codigoCredito = 0;

        for (int i = 0; i < idescredito.size(); i++) {
            codigoCredito = idescredito.get(i);
        }
        codigoCred = codigoCredito;
        con = new Conexion();
        listap = con.obtenerpagos(codigoCredito);
    }

    public void setPagarcredito(Clientes pagarcredito) {
        this.pagarcredito = pagarcredito;

    }

    private Clientes client;

    private Clientes clienthisto;

    public Clientes getClienthisto() {
        return clienthisto;
    }

    public void setClienthisto(Clientes clienthisto) {
        this.clienthisto = clienthisto;

        Conexion con = new Conexion();
        List<Integer> idescredito = new ArrayList();
        idescredito = con.idescredito(clienthisto.getId());

        int codigoCredito = 0;

        for (int i = 0; i < idescredito.size(); i++) {
            codigoCredito = idescredito.get(i);
        }
        codigoCred = codigoCredito;
        con = new Conexion();
        listahisto = con.obtenerpagostotales(codigoCredito);

    }

    private int ano = 1;

    public int getAno() {
        return ano;
    }

    public void setAno(int ano) {
        this.ano = ano;
    }

    public Clientes getClient() {
        return client;
    }

    public void setClient(Clientes client) {
        this.client = client;
        cantidad="";
        prestamo = 0;
        ano = 1;
        lblregla1 = "";
        regla1 = "";
        lblregla2 = "";
        regla2 = "";
        lblregla3 = "";
        regla3 = "";
        lblregla4 = "";
        regla4 = "";
        disable = "true";
        id = client.getId();
        int verificar = 0;
        Conexion con = new Conexion();
        int credito = con.existe(id);
        con = new Conexion();
        String rolpago = con.rolpagos(id);
        con = new Conexion();
        String[] serviciobasico = new String[3];
        serviciobasico = con.serviciosbasicos(id);

        String permiso = "permitir";

        if (rolpago == "") {
            this.cantidad = "";
            this.lblregla1 = "Doc 1:";
            this.regla1 = "Le falta Rol de pago";
            this.disable = "true";
            permiso = "denegar";
        }

        if (serviciobasico[0] == null) {
            this.cantidad = "";
            this.lblregla2 = "Doc 2";
            this.regla2 = "Le falta certificado votacion";
            this.disable = "true";
            permiso = "denegar";
        }

        if (serviciobasico[1] == null) {
            this.cantidad = "";
            this.lblregla3 = "Doc 3";
            this.regla3 = "Le falta los predios";
            this.disable = "true";
            permiso = "denegar";
        }

        if (serviciobasico[2] == null) {
            this.cantidad = "";
            this.lblregla4 = "Doc 4";
            this.regla4 = "Le falta los servicios basicos";
            this.disable = "true";
            permiso = "denegar";
        } else if (!permiso.equals("denegar")) {

            if (credito != 0) {
                con = new Conexion();
                String creditoTotal = con.total(id);
                if (creditoTotal.equals("cancelado")) {
                    con = new Conexion();
                    String[] pagos = con.pagos(credito);
                    for (int i = 0; i < pagos.length; i++) {
                        verificar = Integer.parseInt(pagos[i]);
                        if (verificar <= -1) {

                            con = new Conexion();
                            List<Integer> ides = new ArrayList();
                            ides = con.ides(id);

                            if (ides.size() >= 2) {

                                List<Double> neto = new ArrayList();
                                con = new Conexion();
                                neto=con.listavalorNeto(id);
//                                for (int j = 0; j < ides.size(); j++) {
//                                    
//                                    double valo = con.valorNeto(id);
//                                    neto.add(valo);
//                                }
                                double totalneto = 0;
                                for (int j = 0; j < neto.size(); j++) {

                                    if (j + 1 == neto.size()) {
                                        totalneto = neto.get(j) - neto.get(j - 1);
                                        break;
                                    }
                                }

                                if (totalneto <= 0) {
                                    this.lblregla1 = "Dato 1:";
                                    this.lblregla2 = "Dato 2:";
                                    this.lblregla3 = "";
                                    this.regla1 = "Sus ingresos no han aumentado";
                                    this.regla2 = "Quedo mal los 3 ultimos pagos";
                                    this.regla3 = "";
                                    this.disable = "true";
                                    break;
                                } else {
                                    con = new Conexion();
                                    int index=neto.size()-1;
                                    String maximo = String.valueOf(neto.get(index)*5);
                                    this.cantidad = maximo;
                                    this.lblregla1 = "Dato 1:";
                                    this.lblregla2 = "Dato 2:";
                                    this.lblregla3 = "";
                                    this.regla1 = "Sus ingresos si han aumentado";
                                    this.regla2 = "Quedo mal los 3 ultimos pagos";
                                    this.regla3 = "";
                                    this.disable = "false";
                                    break;
                                }

                            } else {
                                this.lblregla1 = "Dato 1:";
                                this.lblregla2 = "Dato 2:";
                                this.lblregla3 = "";
                                this.regla1 = "Sus ingresos no han aumentado";
                                this.regla2 = "Quedo mal los ultimos 3 pagos";
                                this.regla3 = "";
                                this.disable = "true";
                                break;
                            }

                        } else {
                            con = new Conexion();
                            String maximo = con.maximo(id);
                            this.cantidad = maximo;
                            this.lblregla1 = "";
                            this.lblregla2 = "";
                            this.lblregla3 = "";
                            this.regla1 = "";
                            this.regla2 = "";
                            this.regla3 = "";
                        }
                    }
                    //No se le ofrece ya el 10% mas
                    if (verificar >= 0) {
                        con = new Conexion();
                        String maximo = con.maximo(id);
                        this.cantidad = maximo;
                        this.lblregla1 = "";
                        this.lblregla2 = "";
                        this.lblregla3 = "";
                        this.regla1 = "";
                        this.regla2 = "";
                        this.regla3 = "";
                        this.disable = "true";
                    }
                } else {
                    this.lblregla1 = "Dato:";
                    this.lblregla2 = "No ha terminado de pagar su ultimo credito";
                    this.lblregla3 = "";
                    this.regla1 = "";
                    this.regla2 = "";
                    this.regla3 = "";
                    this.disable = "true";
                }

            } else {
                con = new Conexion();
                String maximo = con.maximo(id);
                this.cantidad = maximo;
                this.lblregla1 = "";
                this.lblregla2 = "";
                this.lblregla3 = "";
                this.regla1 = "";
                this.regla2 = "";
                this.regla3 = "";
                this.disable = "false";
            }
        } else {
            this.disable = "true";
        }
    }

    private String cantidad;
    private String lblregla1, lblregla2, lblregla3, lblregla4, regla4;

    public String getLblregla4() {
        return lblregla4;
    }

    public void setLblregla4(String lblregla4) {
        this.lblregla4 = lblregla4;
    }

    public String getRegla4() {
        return regla4;
    }

    public void setRegla4(String regla4) {
        this.regla4 = regla4;
    }

    public String getLblregla1() {
        return lblregla1;
    }

    public void setLblregla1(String lblregla1) {
        this.lblregla1 = lblregla1;
    }

    public String getLblregla2() {
        return lblregla2;
    }

    public void setLblregla2(String lblregla2) {
        this.lblregla2 = lblregla2;
    }

    public String getLblregla3() {
        return lblregla3;
    }

    public void setLblregla3(String lblregla3) {
        this.lblregla3 = lblregla3;
    }

    public String getRegla1() {
        return regla1;
    }

    public void setRegla1(String regla1) {
        this.regla1 = regla1;
    }

    public String getRegla2() {
        return regla2;
    }

    public void setRegla2(String regla2) {
        this.regla2 = regla2;
    }

    public String getRegla3() {
        return regla3;
    }

    public void setRegla3(String regla3) {
        this.regla3 = regla3;
    }
    private String regla1, regla2, regla3;

    public String getCantidad() {
        return cantidad;
    }

    public void setCantidad(String cantidad) {
        this.cantidad = cantidad;
    }

    public List<Clientes> getLista() {
        return lista;
    }

    public void setLista(List<Clientes> lista) {
        this.lista = lista;
    }

    private String cerrar;

    public String getCerrar() {
        return cerrar;
    }

    public void setCerrar(String cerrar) {
        this.cerrar = cerrar;
    }

    public void guardarCredito() {

        cerrar = "#";
        double cuotas = 0.0;
        Conexion con = new Conexion();
        int numero = con.numero();
        numero = numero + 1;

        Calendar fecha = new GregorianCalendar();
        int ani = fecha.get(Calendar.YEAR);
        int mes = fecha.get(Calendar.MONTH) + 1;
        int dia = fecha.get(Calendar.DAY_OF_MONTH);

        String estado = "pendiente";
        String fechafinal = String.valueOf(dia + "-" + mes + "-" + ani);
        String fechaLimite = String.valueOf(dia + "-" + mes + "-" + (ani + ano));
        con = new Conexion();

        String logrado = "si";

        if (ano >= 31) {
            lblregla2 = "Dato";
            regla2 = "Años no deben superar los 30";
            logrado = "no";
        }

        if (prestamo > Double.parseDouble(cantidad)) {
            lblregla1 = "Dato";
            regla1 = "El prestamo no puede superar la cantidad maxima a prestar";
            logrado = "no";
        }

        if (logrado == "si") {
            con.guardarCredito(numero, fechafinal, prestamo, estado, fechaLimite, id);

            lblregla2 = "";
            regla2 = "";
            lblregla1 = "";
            regla1 = "";

            con = new Conexion();
            int credito = con.existe(id);

            if (ano != 0) {
                cuotas = (prestamo / (ano * 11));
                cuotas = Math.round(cuotas * 100);
                cuotas = cuotas / 100;
            }

            String fech = "";
            for (int i = 0; i < (ano * 12); i++) {

                con = new Conexion();
                int idpagos = con.numeropagos();

                if (mes == 0) {
                    fech = String.valueOf(dia + "-" + (mes + 1) + "-" + ani);
                    mes++;
                } else {
                    fech = String.valueOf(dia + "-" + (mes + 1) + "-" + ani);
                    mes++;
                }

                if (mes == 12) {
                    mes = 0;
                    ani++;
                }
                con = new Conexion();
                con.guardarPago((idpagos + 1), cuotas, estado, fech, credito);
            }
            lblregla1 = "Dato";
            regla1 = "Ya se realizo el credito";
            disable = "true";
        }
    }

    @PostConstruct
    public void init() {
        Conexion con = new Conexion();
        lista = con.obtenerClientes();
        con = new Conexion();
        listapp = con.obtenerClientespago();
        //GuardarCredito();
    }

}
