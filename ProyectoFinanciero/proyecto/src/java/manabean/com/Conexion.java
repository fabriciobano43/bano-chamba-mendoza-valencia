/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package manabean.com;

import com.modelo.Clientes;
import com.modelo.Historal;
import com.modelo.Pagar;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author JoseRene
 */
public class Conexion {
    
    public Connection connection;
    private String message;
    private Statement statement;
    ResultSet resultSet;
    
    public Conexion() {
        try {
            //Class.forName("com.mysql.jdbc.Driver");
            //connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/proyectofinanciero", "root", "");
            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/Finan", "postgres", "12345");
            statement = connection.createStatement
            (ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
            //connection = DriverManager.getConnection(url, username, password);
            message="ok";
        } catch (Exception ex) {
            message=ex.getMessage();
        }
    }
        
    public List<Clientes> obtenerClientes() {
        List<Clientes> lista = new ArrayList();

        int n = 0;
        try {
            ResultSet rs = statement.executeQuery("select idpersona, cedula,nombre,apellido,direccion from persona");
            while (rs.next()) {
                Clientes c = new Clientes();
                int id = Integer.parseInt(rs.getString("idpersona"));
                String cedula = rs.getString("cedula");
                String nombre = rs.getString("nombre");
                String apellido = rs.getString("apellido");
                String direccion = rs.getString("direccion");
                c.setApellido(apellido);
                c.setId(id);
                c.setCedula(cedula);
                c.setDireccion(direccion);
                c.setNombre(nombre);
                lista.add(n, c);
                n++;
            }
            rs.close();
            statement.close();
            connection.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
        return lista;
    }

    public List<Pagar> obtenerClientespago() {
        List<Pagar> lista = new ArrayList();

        int n = 0;
        try {
            ResultSet rs = statement.executeQuery("select p.idpersona as id, p.cedula as cedula, p.nombre as nombre, p.apellido as apellido, \n"
                    + "p.direccion as direccion \n"
                    + "from credito c inner join persona p \n"
                    + "on c.idpersona=p.idpersona where c.estado='pendiente'");
            while (rs.next()) {
                Pagar c = new Pagar();
                int id = Integer.parseInt(rs.getString("id"));
                String cedula = rs.getString("cedula");
                String nombre = rs.getString("nombre");
                String apellido = rs.getString("apellido");
                String direccion = rs.getString("direccion");
                c.setApellido(apellido);
                c.setId(id);
                c.setCedula(cedula);
                c.setDireccion(direccion);
                c.setNombre(nombre);
                lista.add(n, c);
                n++;
            }
            rs.close();
            statement.close();
            connection.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
        return lista;
    }

    public List<Pagar> obtenerpagos(int codigo) {
        List<Pagar> lista = new ArrayList();
        int n = 0;
        try {
            ResultSet rs = statement.executeQuery("select idpagos,cuota,estado,fecha_a_pagar from pagos where idcredito=" + codigo + " and estado='pendiente'");
            while (rs.next()) {
                Pagar c = new Pagar();
                int id = Integer.parseInt(rs.getString("idpagos"));
                double cuota = Double.parseDouble(rs.getString("cuota"));
                String estado = rs.getString("estado");
                String fecha = rs.getString("fecha_a_pagar");
                c.setCuota(cuota);
                c.setId(id);
                c.setEstado(estado);
                c.setFecha(fecha);
                lista.add(n, c);
                n++;
            }
            rs.close();
            statement.close();
            connection.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
        return lista;
    }

    public List<Historal> obtenerpagostotales(int codigo) {
        List<Historal> lista = new ArrayList();

        int n = 0;
        try {
            ResultSet rs = statement.executeQuery("select idpagos,fechapago,cuota,estado,fecha_a_pagar from pagos where idcredito=" + codigo + "");
            while (rs.next()) {
                Historal c = new Historal();
                int id = Integer.parseInt(rs.getString("idpagos"));
                double cuota = Double.parseDouble(rs.getString("cuota"));
                String estado = rs.getString("estado");
                String fecha = rs.getString("fecha_a_pagar");
                String fechapague = rs.getString("fechapago");
                c.setCuota(cuota);
                c.setId(id);
                c.setEstado(estado);
                c.setFechapagar(fecha);
                c.setFechadepago(fechapague);
                lista.add(n, c);
                n++;
            }
            rs.close();
            statement.close();
            connection.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
        return lista;
    }

    public String[] iniciar() {
        String datos[] = new String[2];
        int n = 0;
        try {
            ResultSet rs = statement.executeQuery("select usuario, clave from usuario");
            while (rs.next()) {
                datos[n] = rs.getString("usuario");
                n++;
                datos[n] = rs.getString("clave");
            }
            rs.close();
            statement.close();
            connection.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
        return datos;
    }

    
     public String[] serviciosbasicos(int id) {
        String datos[] = new String[3];
        int n = 0;
        try {
            ResultSet rs = statement.executeQuery("select votacion, prediosoarrendamiento,serviciosbasicos from certificados where idpersona="+id+"");
            while (rs.next()) {
                datos[n] = rs.getString("votacion");
                n++;
                datos[n] = rs.getString("prediosoarrendamiento");
                n++;
                datos[n]=rs.getString("serviciosbasicos");
                n=0;
            }
            rs.close();
            statement.close();
            connection.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
        return datos;
    }
    
    
    
    
    public String maximo(int id) {
        double valor = 0;
        try {
            ResultSet rs = statement.executeQuery("select ingresos from ingresos where idpersona=" + id + "");
            while (rs.next()) {
                valor = Double.parseDouble(rs.getString("ingresos"));
                valor = valor * 10;
            }
            rs.close();
            statement.close();
            connection.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
        return String.valueOf(valor);
    }
    
    
    public String rolpagos(int id) {
        String valor = "";
        try {
            ResultSet rs = statement.executeQuery("select rolpagos from ingresos where idpersona=" + id + "");
            while (rs.next()) {
                valor = rs.getString("rolpagos");
            }
            rs.close();
            statement.close();
            connection.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
        }
        return valor;
    }

    public String obtenerfechapago(int id) {
        String fecha = "";
        try {
            ResultSet rs = statement.executeQuery("select fecha_a_pagar from pagos where idpagos=" + id + "");
            while (rs.next()) {
                fecha = rs.getString("fecha_a_pagar");
            }
            rs.close();
            statement.close();
            connection.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
        }
        return fecha;
    }

    public String maximoL(int id) {
        int valor = 0;
        try {
            ResultSet rs = statement.executeQuery("select ingresos from ingresos where idpersona=" + id + "");
            while (rs.next()) {
                valor = Integer.parseInt(rs.getString("ingresos"));
                valor = valor * 5;
            }
            rs.close();
            statement.close();
            connection.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
        return String.valueOf(valor);
    }

    public int existe(int id) {
        int ide = 0;
        try {
            ResultSet rs = statement.executeQuery("select idcredito from credito where idpersona=" + id + "");
            while (rs.next()) {
                ide = Integer.parseInt(rs.getString("idcredito"));
            }
            rs.close();
            statement.close();
            connection.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
        return ide;
    }

    public int numero() {
        int ide = 0;
        try {
            ResultSet rs = statement.executeQuery("select Max(idcredito) as idcre from credito");
            while (rs.next()) {
                ide = Integer.parseInt(rs.getString("idcre"));
            }
            rs.close();
            statement.close();
            connection.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
        }
        return ide;
    }

    public int numeropagos() {
        int ide = 0;
        try {
            ResultSet rs = statement.executeQuery("select Max(idpagos) as idcre from pagos");
            while (rs.next()) {
                ide = Integer.parseInt(rs.getString("idcre"));
            }
            rs.close();
            statement.close();
            connection.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
        }
        return ide;
    }

    public String[] pagos(int credito) {
        int ide = 0;
        String[] pagos = new String[3];
        try {
            ResultSet rs = statement.executeQuery("select (fecha_a_pagar-fechapago) as tiempo from pagos where idcredito=" + credito + " and estado='cancelado' order by idpagos desc limit 3");
            while (rs.next()) {
                String pago = rs.getString("tiempo");
                pagos[ide] = pago;
                ide++;
            }
            rs.close();
            statement.close();
            connection.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
        return pagos;
    }

    public String total(int id) {
        int ide = 0;
        String credito = "";
        try {
            ResultSet rs = statement.executeQuery("select estado from credito where idpersona=" + id + "");
            while (rs.next()) {
                credito = rs.getString("estado");
            }
            rs.close();
            statement.close();
            connection.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
        return credito;
    }

    public String cedula(int id) {
        int ide = 0;
        String cedula = "";
        try {
            ResultSet rs = statement.executeQuery("select cedula from persona where idpersona=" + id + "");
            while (rs.next()) {
                cedula = rs.getString("cedula");
            }
            rs.close();
            statement.close();
            connection.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
        }
        return cedula;
    }

    public String fechalimite(int id) {
        String fechalimi = "";
        try {
            ResultSet rs = statement.executeQuery("select fechalimite from credito where idcredito=" + id + "");
            while (rs.next()) {
                fechalimi = rs.getString("fechalimite");
            }
            rs.close();
            statement.close();
            connection.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
        }
        return fechalimi;
    }

    public void actualizarpago(String fecha, int id) {
        int ide = 0;
        String cedula = "";
        try {
            ResultSet rs = statement.executeQuery("update pagos set fechapago='" + fecha + "',estado='cancelado' where idpagos=" + id + "");
            rs.close();
            statement.close();
            connection.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
        }
    }

    public void actualizarcredito(int id, String fecha) {
        int ide = 0;
        String cedula = "";
        try {
            ResultSet rs = statement.executeQuery("update credito set estado='cancelado',fechapagocompletadp='" + fecha + "' where idcredito=" + id + "");
            rs.close();
            statement.close();
            connection.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
        }
    }

    public List<Integer> ides(int cedula) {
        int cedulaf = 0;
        List<Integer> lista = new ArrayList();
        try {
            ResultSet rs = statement.executeQuery("select idingresos from ingresos where idpersona=" + cedula + "");
            while (rs.next()) {
                cedulaf = Integer.parseInt(rs.getString("idingresos"));
                lista.add(cedulaf);
            }
            rs.close();
            statement.close();
            connection.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
        }
        return lista;
    }

    public List<Integer> idescredito(int id) {
        int idf = 0;
        List<Integer> lista = new ArrayList();
        try {
            ResultSet rs = statement.executeQuery("select idcredito from credito where idpersona=" + id + "");
            while (rs.next()) {
                idf = Integer.parseInt(rs.getString("idcredito"));
                lista.add(idf);
            }
            rs.close();
            statement.close();
            connection.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
        }
        return lista;
    }

    public double valorNeto(int id) {
        double valor = 0;
        try {
            ResultSet rs = statement.executeQuery("select ingresos from ingresos where idpersona=" + id + "");
            while (rs.next()) {
                valor = Double.parseDouble(rs.getString("ingresos"));
            }
            rs.close();
            statement.close();
            connection.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
        }
        return valor;
    }
    
    public List<Double> listavalorNeto(int id) {
        List<Double> valo =new ArrayList();
        double valor=0;
        int n=0;
        try {
            ResultSet rs = statement.executeQuery("select ingresos from ingresos where idpersona=" + id + "");
            while (rs.next()) {
                valor = Double.parseDouble(rs.getString("ingresos"));
                valo.add(n, valor);
                n++;
            }
            rs.close();
            statement.close();
            connection.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
        }
        return valo;
    }
    
    
    
    
    
    
//     public double valornetototal(int id) {
//        double valor = 0;
//        try {
//            ResultSet rs = statement.executeQuery("select ingresos from ingresos where idpersona=" + id + "");
//            while (rs.next()) {
//                valor = Double.parseDouble(rs.getString("ingresos"));
//            }
//            rs.close();
//            statement.close();
//            connection.close();
//        } catch (Exception e) {
//            System.err.println(e.getClass().getName() + ": " + e.getMessage());
//        }
//        return valor;
//    }
    
    
    
    
    
    

    public void guardarCredito(int id, String fehca, Double dinero, String estado, String anio, int idpersona) {
        try {
            ResultSet rs = statement.executeQuery("insert into credito values (" + id + ",'" + fehca + "','" + dinero + "','" + estado + "','" + anio + "','" + anio + "'," + idpersona + ")");
            rs.close();
            statement.close();
            connection.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
        }
    }

    public void guardarPago(int id, Double dinero, String estado, String anio, int idcredito) {
        try {
            ResultSet rs = statement.executeQuery("insert into pagos values(" + id + ",null," + dinero + ",'" + anio + "', '" + estado + "'," + idcredito + ")");
            rs.close();
            statement.close();
            connection.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
        }
    }
      
     
     
}
