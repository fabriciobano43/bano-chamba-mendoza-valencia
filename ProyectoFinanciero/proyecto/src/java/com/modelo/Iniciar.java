/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.modelo;

/**
 *
 * @author JoseRene
 */

import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import manabean.com.Conexion;

@ManagedBean
@SessionScoped
public class Iniciar implements Serializable{
    
      private String usuario, clave;

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }
    
    public String iniciarSesion(){
        Conexion con=new Conexion();
        String []datos=new String[2];
        String ruta="#";
        datos=con.iniciar();        
        if(datos[0].equals(usuario)&&datos[1].equals(clave))
        {
           ruta="Inicio.xhtml";
        }
        return ruta;
    }
    
}
