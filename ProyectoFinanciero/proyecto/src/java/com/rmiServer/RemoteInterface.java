/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rmiServer;

/**
 *
 * @author gugle
 */
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;

// Creating Remote interface for our application
public interface RemoteInterface extends Remote {
    
    public int countCliente() throws RemoteException;
    public String insertarCliente(int idCliente, String nombre, String apellido, String genero, String direccion, String cedula) throws RemoteException;
    public int countRolPagos() throws RemoteException;
    public String insertarRolPagos(int idIngreso, String rolPagos, String fecha, int idCliente, double ingreso) throws RemoteException;
    public int countCertificado() throws RemoteException;
    public String insertarCertificado(int idCertificado, String certificadoVotacion, String prediosArrendamiento, String fecha, int idCliente, String impuesto, String servicioBasico) throws  RemoteException;
    public ArrayList buscarCliente(String cedula) throws RemoteException;

}