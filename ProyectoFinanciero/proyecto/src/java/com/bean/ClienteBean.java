/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template certificadoVotacion, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bean;

import com.rmiServer.RemoteInterface;
import com.utils.Archivo;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import org.primefaces.model.file.UploadedFile;

/**
 *
 * @author Javier
 */
@RequestScoped
@ManagedBean
public class ClienteBean implements Serializable {

    private final RemoteInterface stub;
    private String nombre, apellido, genero, direccion, message, cedula, cliente;
    private int idCliente, idIngreso, idCertificado;
    private Date date = new Date();
    private UploadedFile certificadoVotacion, rolPagos, impuesto, serviciosBasicos, prediosArrendamiento;
    private double ingreso;
    private List<String> registros;

    Archivo archivo = new Archivo();
    ServletContext ctx = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext(); //Obtengo la ruta de la aplicación

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public UploadedFile getServiciosBasicos() {
        return serviciosBasicos;
    }

    public void setServiciosBasicos(UploadedFile serviciosBasicos) {
        this.serviciosBasicos = serviciosBasicos;
    }

    public UploadedFile getPrediosArrendamiento() {
        return prediosArrendamiento;
    }

    public void setPrediosArrendamiento(UploadedFile prediosArrendamiento) {
        this.prediosArrendamiento = prediosArrendamiento;
    }

    public int getIdCertificado() {
        return idCertificado;
    }

    public void setIdCertificado(int idCertificado) {
        this.idCertificado = idCertificado;
    }

    public UploadedFile getImpuesto() {
        return impuesto;
    }

    public void setImpuesto(UploadedFile impuesto) {
        this.impuesto = impuesto;
    }

    public int getIdIngreso() {
        return idIngreso;
    }

    public void setIdIngreso(int idIngreso) {
        this.idIngreso = idIngreso;
    }

    public double getIngreso() {
        return ingreso;
    }

    public void setIngreso(double ingreso) {
        this.ingreso = ingreso;
    }

    public int getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public UploadedFile getRolPagos() {
        return rolPagos;
    }

    public void setRolPagos(UploadedFile rolPagos) {
        this.rolPagos = rolPagos;
    }

    public UploadedFile getCertificadoVotacion() {
        return certificadoVotacion;
    }

    public void setCertificadoVotacion(UploadedFile certificadoVotacion) {
        this.certificadoVotacion = certificadoVotacion;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public void buscar() throws RemoteException {
        registros = stub.buscarCliente(cedula);
        cliente = registros.get(1) + " " + registros.get(2);
    }

    public void registrarCertificados() throws RemoteException, IOException, InterruptedException {
        if (archivo.validarCedula(cedula)) {
            message = "registrarCertificados";
            if (certificadoVotacion!=null && !certificadoVotacion.getFileName().equals("")) { //Pregunta si hay subido un archivo en el certificado de votación
                if (prediosArrendamiento!=null && !prediosArrendamiento.getFileName().equals("")) { //Pregunta si ha subido el archivo de certificado de predio o contrato de arrendamiento
                    if (impuesto!=null && !impuesto.getFileName().equals("")) { //Pregunta si ha subido el archivo de servicios básicos
                        if (!serviciosBasicos.getFileName().equals("")) {//Pregunta si ha subido el archivo de certificado de predio o contrato de arrendamiento
                            action(message);
                            FacesMessage msg = new FacesMessage("Cliente registrado correctamente");
                            FacesContext.getCurrentInstance().addMessage(null, msg);
                            limpiecito();
                        } else {
                            FacesMessage msg = new FacesMessage("Por favor, suba el archivo de los servicios básicos");
                            FacesContext.getCurrentInstance().addMessage(null, msg);
                            limpiecito();
                        }
                    } else {
                        FacesMessage msg = new FacesMessage("Por favor, suba el archivo de los impuestos");
                        FacesContext.getCurrentInstance().addMessage(null, msg);
                        limpiecito();
                    }
                } else {
                    FacesMessage msg = new FacesMessage("Por favor, suba el archivo de certificado de predios o contrato de arrendamiento");
                    FacesContext.getCurrentInstance().addMessage(null, msg);
                    limpiecito();
                }
            } else {
                FacesMessage msg = new FacesMessage("Por favor, suba el archivo del certificado de votación");
                FacesContext.getCurrentInstance().addMessage(null, msg);
                limpiecito();
            }
        } else {
            FacesMessage msg = new FacesMessage("Por favor, ingrese correctamente el número de cédula");
            FacesContext.getCurrentInstance().addMessage(null, msg);
            limpiecito();
        }
    }

    public void registrarRolPagos() throws RemoteException, IOException, InterruptedException {
        if (archivo.validarCedula(cedula)) {
            message = "registrarRolPagos";
            if (rolPagos != null && !rolPagos.getFileName().equals("") && ingreso != 0.0) { //Pregunta si hay arhivo de rol de pagos
                action(message);
                FacesMessage msg = new FacesMessage("Rol de pagos registrado correctamente");
                FacesContext.getCurrentInstance().addMessage(null, msg);
                limpiecito();
                
            } else {
                FacesMessage msg = new FacesMessage("Por favor, suba el archivo del rol de pagos");
                FacesContext.getCurrentInstance().addMessage(null, msg);
                limpiecito();
            }
        } else {
            FacesMessage msg = new FacesMessage("Por favor, ingrese correctamente el número de cédula");
            FacesContext.getCurrentInstance().addMessage(null, msg);
            limpiecito();
        }
    }

    public void registrarCliente() throws RemoteException, IOException, InterruptedException {
        idCliente = stub.countCliente(); //Contador del cliente
        if (archivo.validarCedula(cedula)) {
            message = "registrarCliente";
            action(message);
            FacesMessage msg = new FacesMessage("Cliente registrado correctamente");
            FacesContext.getCurrentInstance().addMessage(null, msg);
            limpiar();

        } else {
            FacesMessage msg = new FacesMessage("Por favor, ingrese correctamente el número de cédula");
            FacesContext.getCurrentInstance().addMessage(null, msg);
            limpiar();
        }
    }

    public void limpiecito(){
    nombre=""; 
                            apellido="";
                            genero=""; 
                            direccion=""; 
                            message=""; 
                            cedula=""; 
                            cliente="";
                            ingreso = 0.0;
                            certificadoVotacion=null;
                            rolPagos=null;
                            impuesto=null; 
                            serviciosBasicos=null;
                            prediosArrendamiento=null;
}
    public void limpiar() {
        cedula = "";
        nombre = "";
        apellido = "";
        genero = "";
        direccion = "";
        ingreso = 0.0;
    }

    public ClienteBean() throws RemoteException, NotBoundException {
        Registry registry;
        registry = LocateRegistry.getRegistry(8888);
        // Looking up the registry for the remote object
        stub = (RemoteInterface) registry.lookup("RMI");
    }

    public void action(String message) {
        try {
            String rutaCarpetaJava = ctx.getRealPath("/").replace("\\build\\web\\", "\\web\\cliente\\"); //Cambio la direccion donde se va a guardar los doc
            DateFormat hourdateFormat = new SimpleDateFormat("ddMMyyyyHHmmss"); //Fecha y hora de subida del docs
            DateFormat fecha = new SimpleDateFormat("yyyy/MM/dd");
            String ivaRenta = null;

            if (message.equals("registrarCliente")) {
                message = stub.insertarCliente(idCliente, nombre, apellido, genero, direccion, cedula);//Almaceno a la BD

                File directorio = new File(rutaCarpetaJava + idCliente); //La ruta de directorio y el identificador de cada cliente
                directorio.mkdirs(); //Creo el directorio
            } else if (message.equals("registrarRolPagos")) {
                registros = stub.buscarCliente(cedula);
                idCliente = Integer.parseInt(registros.get(0));

                rutaCarpetaJava += idCliente + "\\"; //La ruta donde se almacena los docs del cliente

                if (!rolPagos.getFileName().equals("")) {
                    //ROL DE PAGOS
                    idIngreso = stub.countRolPagos(); //Contador del rol de pagos
                    String nombreRolPagos = idCliente + "-RP-" + String.valueOf(hourdateFormat.format(date) + ".pdf"); //Nombre concatenado con identificador, RP (Rol de pagos), fecha y hora, extensión del archivo
                    InputStream rutaRolPagos = rolPagos.getInputStream(); //El InputStream del archivo
                    message = stub.insertarRolPagos(idIngreso, "/cliente/" + idCliente + "/" + nombreRolPagos, fecha.format(date), idCliente, ingreso); //Almaceno a la BD
                    if (archivo.upload(rutaRolPagos, rutaCarpetaJava + nombreRolPagos)) //Función para copiar archivo hacia la nueva carpeta de la aplicación
                    {
                        System.out.println("Rol de pagos aceptada");
                    } else {
                        System.out.println("Rol de pagos rechazada");
                    }
                }

            } else if (message.equals("registrarCertificados")) {
                registros = stub.buscarCliente(cedula);
                idCliente = Integer.parseInt(registros.get(0));

                rutaCarpetaJava += idCliente + "\\"; //La ruta donde se almacena los docs del cliente

                //CERTIFICADO DE VOTACIÓN
                idCertificado = stub.countCertificado();
                String nombreCertificadoVotacion = idCliente + "-CV-" + String.valueOf(hourdateFormat.format(date) + ".pdf"); //Nombre concatenado con identificador, CV (Certificado de votación), fecha y hora, extensión del archivo
                InputStream rutaCertificadoVotacion = certificadoVotacion.getInputStream(); //El InputStream del archivo
                if (archivo.upload(rutaCertificadoVotacion, rutaCarpetaJava + nombreCertificadoVotacion)) //Función para copiar archivo hacia la nueva carpeta de la aplicación
                {
                    System.out.println("Certificado de votación aceptada");
                } else {
                    System.out.println("Certificado de votación rechazada");
                }

                if (!impuesto.getFileName().equals("")) {
                    //IMPUESTO (IVA Y RENTA)
                    String nombreImpuesto = idCliente + "-IR-" + String.valueOf(hourdateFormat.format(date) + ".pdf"); //Nombre concatenado con identificador, IR (IVA Y RENTA), fecha y hora, extensión del archivo
                    InputStream rutaImpuesto = impuesto.getInputStream(); //El InputStream del archivo
                    if (archivo.upload(rutaImpuesto, rutaCarpetaJava + nombreImpuesto)) //Función para copiar archivo hacia la nueva carpeta de la aplicación
                    {
                        System.out.println("Impuesto aceptado");
                    } else {
                        System.out.println("Impuesto rechazado");
                    }
                    ivaRenta = "/cliente/" + idCliente + "/" + nombreImpuesto;

                }

                //SERVICIOS BÁSICOS
                String nombreServiciosBasicos = idCliente + "-SB-" + String.valueOf(hourdateFormat.format(date) + ".pdf"); //Nombre concatenado con identificador, SB (Servicios Básicos), fecha y hora, extensión del archivo
                InputStream rutaServiciosBasicos = serviciosBasicos.getInputStream(); //El InputStream del archivo
                if (archivo.upload(rutaServiciosBasicos, rutaCarpetaJava + nombreServiciosBasicos)) //Función para copiar archivo hacia la nueva carpeta de la aplicación
                {
                    System.out.println("Servicios básicos aceptados");
                } else {
                    System.out.println("Servicios básicos rechazados");
                }

                //Certificados de predios o contrato de arrendamiento
                String nombrePrediosArrendamiento = idCliente + "-PA-" + String.valueOf(hourdateFormat.format(date) + ".pdf"); //Nombre concatenado con identificador, PA (Predios o Arrendamiento), fecha y hora, extensión del archivo
                InputStream rutaPrediosArrendamiento = serviciosBasicos.getInputStream(); //El InputStream del archivo
                if (archivo.upload(rutaPrediosArrendamiento, rutaCarpetaJava + nombrePrediosArrendamiento)) //Función para copiar archivo hacia la nueva carpeta de la aplicación
                {
                    System.out.println("Certificados de predios o contrato de arrendamiento aceptado");
                } else {
                    System.out.println("Certificados de predios o contrato de arrendamiento rechazado");
                }

                message = stub.insertarCertificado(idCertificado, "/cliente/" + idCliente + "/" + nombreCertificadoVotacion, "/cliente/" + idCliente + "/" + nombrePrediosArrendamiento, fecha.format(date), idCliente, ivaRenta, "/cliente/" + idCliente + "/" + nombreServiciosBasicos); //Almaceno a la BD
            }

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

}
