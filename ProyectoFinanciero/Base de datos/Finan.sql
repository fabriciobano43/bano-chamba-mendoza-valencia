PGDMP     ,    6                x            Finan    12.3    12.3 .    F           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false            G           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false            H           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false            I           1262    24668    Finan    DATABASE     �   CREATE DATABASE "Finan" WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'Spanish_Ecuador.1252' LC_CTYPE = 'Spanish_Ecuador.1252';
    DROP DATABASE "Finan";
                postgres    false            �            1259    24669    certificados    TABLE     �  CREATE TABLE public.certificados (
    idcertificados integer NOT NULL,
    votacion character varying(250) DEFAULT NULL::character varying,
    prediosoarrendamiento character varying(250) DEFAULT NULL::character varying,
    fecha date,
    idpersona integer,
    ivaorenta character varying(250) DEFAULT NULL::character varying,
    serviciosbasicos character varying(250) DEFAULT NULL::character varying
);
     DROP TABLE public.certificados;
       public         heap    postgres    false            �            1259    24679    certificados_idcertificados_seq    SEQUENCE     �   CREATE SEQUENCE public.certificados_idcertificados_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 6   DROP SEQUENCE public.certificados_idcertificados_seq;
       public          postgres    false    202            J           0    0    certificados_idcertificados_seq    SEQUENCE OWNED BY     c   ALTER SEQUENCE public.certificados_idcertificados_seq OWNED BY public.certificados.idcertificados;
          public          postgres    false    203            �            1259    24681    credito    TABLE     $  CREATE TABLE public.credito (
    idcredito integer NOT NULL,
    fechainicio date,
    total character varying(45) DEFAULT NULL::character varying,
    estado character varying(45) DEFAULT NULL::character varying,
    fechalimite date,
    fechapagocompletadp date,
    idpersona integer
);
    DROP TABLE public.credito;
       public         heap    postgres    false            �            1259    24686    credito_idcredito_seq    SEQUENCE     �   CREATE SEQUENCE public.credito_idcredito_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ,   DROP SEQUENCE public.credito_idcredito_seq;
       public          postgres    false    204            K           0    0    credito_idcredito_seq    SEQUENCE OWNED BY     O   ALTER SEQUENCE public.credito_idcredito_seq OWNED BY public.credito.idcredito;
          public          postgres    false    205            �            1259    24688    ingresos    TABLE     �   CREATE TABLE public.ingresos (
    idingresos integer NOT NULL,
    rolpagos character varying(250) DEFAULT NULL::character varying,
    fecha date,
    idpersona integer,
    ingresos numeric
);
    DROP TABLE public.ingresos;
       public         heap    postgres    false            �            1259    24695    ingresos_idingresos_seq    SEQUENCE     �   CREATE SEQUENCE public.ingresos_idingresos_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.ingresos_idingresos_seq;
       public          postgres    false    206            L           0    0    ingresos_idingresos_seq    SEQUENCE OWNED BY     S   ALTER SEQUENCE public.ingresos_idingresos_seq OWNED BY public.ingresos.idingresos;
          public          postgres    false    207            �            1259    24697    pagos    TABLE     �   CREATE TABLE public.pagos (
    idpagos integer NOT NULL,
    fechapago date,
    cuota numeric,
    fecha_a_pagar date,
    estado character varying(45) DEFAULT NULL::character varying,
    idcredito integer
);
    DROP TABLE public.pagos;
       public         heap    postgres    false            �            1259    24704    pagos_idpagos_seq    SEQUENCE     �   CREATE SEQUENCE public.pagos_idpagos_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.pagos_idpagos_seq;
       public          postgres    false    208            M           0    0    pagos_idpagos_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE public.pagos_idpagos_seq OWNED BY public.pagos.idpagos;
          public          postgres    false    209            �            1259    24706    persona    TABLE       CREATE TABLE public.persona (
    idpersona integer NOT NULL,
    cedula character varying(10) NOT NULL,
    nombre character varying(25) NOT NULL,
    apellido character varying(30) NOT NULL,
    genero character varying(2) NOT NULL,
    direccion character varying(50) NOT NULL
);
    DROP TABLE public.persona;
       public         heap    postgres    false            �            1259    24709    usuario    TABLE     }   CREATE TABLE public.usuario (
    id integer NOT NULL,
    usuario character varying(30),
    clave character varying(30)
);
    DROP TABLE public.usuario;
       public         heap    postgres    false            �            1259    24712    usuario_id_seq    SEQUENCE     �   CREATE SEQUENCE public.usuario_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE public.usuario_id_seq;
       public          postgres    false    211            N           0    0    usuario_id_seq    SEQUENCE OWNED BY     A   ALTER SEQUENCE public.usuario_id_seq OWNED BY public.usuario.id;
          public          postgres    false    212            �
           2604    24714    certificados idcertificados    DEFAULT     �   ALTER TABLE ONLY public.certificados ALTER COLUMN idcertificados SET DEFAULT nextval('public.certificados_idcertificados_seq'::regclass);
 J   ALTER TABLE public.certificados ALTER COLUMN idcertificados DROP DEFAULT;
       public          postgres    false    203    202            �
           2604    24715    credito idcredito    DEFAULT     v   ALTER TABLE ONLY public.credito ALTER COLUMN idcredito SET DEFAULT nextval('public.credito_idcredito_seq'::regclass);
 @   ALTER TABLE public.credito ALTER COLUMN idcredito DROP DEFAULT;
       public          postgres    false    205    204            �
           2604    24716    ingresos idingresos    DEFAULT     z   ALTER TABLE ONLY public.ingresos ALTER COLUMN idingresos SET DEFAULT nextval('public.ingresos_idingresos_seq'::regclass);
 B   ALTER TABLE public.ingresos ALTER COLUMN idingresos DROP DEFAULT;
       public          postgres    false    207    206            �
           2604    24717    pagos idpagos    DEFAULT     n   ALTER TABLE ONLY public.pagos ALTER COLUMN idpagos SET DEFAULT nextval('public.pagos_idpagos_seq'::regclass);
 <   ALTER TABLE public.pagos ALTER COLUMN idpagos DROP DEFAULT;
       public          postgres    false    209    208            �
           2604    24718 
   usuario id    DEFAULT     h   ALTER TABLE ONLY public.usuario ALTER COLUMN id SET DEFAULT nextval('public.usuario_id_seq'::regclass);
 9   ALTER TABLE public.usuario ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    212    211            9          0    24669    certificados 
   TABLE DATA           �   COPY public.certificados (idcertificados, votacion, prediosoarrendamiento, fecha, idpersona, ivaorenta, serviciosbasicos) FROM stdin;
    public          postgres    false    202   6       ;          0    24681    credito 
   TABLE DATA           u   COPY public.credito (idcredito, fechainicio, total, estado, fechalimite, fechapagocompletadp, idpersona) FROM stdin;
    public          postgres    false    204   �6       =          0    24688    ingresos 
   TABLE DATA           T   COPY public.ingresos (idingresos, rolpagos, fecha, idpersona, ingresos) FROM stdin;
    public          postgres    false    206   97       ?          0    24697    pagos 
   TABLE DATA           \   COPY public.pagos (idpagos, fechapago, cuota, fecha_a_pagar, estado, idcredito) FROM stdin;
    public          postgres    false    208   
8       A          0    24706    persona 
   TABLE DATA           Y   COPY public.persona (idpersona, cedula, nombre, apellido, genero, direccion) FROM stdin;
    public          postgres    false    210   �9       B          0    24709    usuario 
   TABLE DATA           5   COPY public.usuario (id, usuario, clave) FROM stdin;
    public          postgres    false    211   �:       O           0    0    certificados_idcertificados_seq    SEQUENCE SET     N   SELECT pg_catalog.setval('public.certificados_idcertificados_seq', 1, false);
          public          postgres    false    203            P           0    0    credito_idcredito_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('public.credito_idcredito_seq', 1, false);
          public          postgres    false    205            Q           0    0    ingresos_idingresos_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('public.ingresos_idingresos_seq', 1, false);
          public          postgres    false    207            R           0    0    pagos_idpagos_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('public.pagos_idpagos_seq', 1, false);
          public          postgres    false    209            S           0    0    usuario_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public.usuario_id_seq', 1, false);
          public          postgres    false    212            �
           2606    24720    certificados certificados_pkey 
   CONSTRAINT     h   ALTER TABLE ONLY public.certificados
    ADD CONSTRAINT certificados_pkey PRIMARY KEY (idcertificados);
 H   ALTER TABLE ONLY public.certificados DROP CONSTRAINT certificados_pkey;
       public            postgres    false    202            �
           2606    24722    credito credito_pkey 
   CONSTRAINT     Y   ALTER TABLE ONLY public.credito
    ADD CONSTRAINT credito_pkey PRIMARY KEY (idcredito);
 >   ALTER TABLE ONLY public.credito DROP CONSTRAINT credito_pkey;
       public            postgres    false    204            �
           2606    24724    ingresos ingresos_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.ingresos
    ADD CONSTRAINT ingresos_pkey PRIMARY KEY (idingresos);
 @   ALTER TABLE ONLY public.ingresos DROP CONSTRAINT ingresos_pkey;
       public            postgres    false    206            �
           2606    24726    pagos pagos_pkey 
   CONSTRAINT     S   ALTER TABLE ONLY public.pagos
    ADD CONSTRAINT pagos_pkey PRIMARY KEY (idpagos);
 :   ALTER TABLE ONLY public.pagos DROP CONSTRAINT pagos_pkey;
       public            postgres    false    208            �
           2606    24728    persona persona_pkey 
   CONSTRAINT     Y   ALTER TABLE ONLY public.persona
    ADD CONSTRAINT persona_pkey PRIMARY KEY (idpersona);
 >   ALTER TABLE ONLY public.persona DROP CONSTRAINT persona_pkey;
       public            postgres    false    210            �
           2606    24730    usuario usuario_pkey 
   CONSTRAINT     R   ALTER TABLE ONLY public.usuario
    ADD CONSTRAINT usuario_pkey PRIMARY KEY (id);
 >   ALTER TABLE ONLY public.usuario DROP CONSTRAINT usuario_pkey;
       public            postgres    false    211            �
           2606    24731    pagos creditos_pago    FK CONSTRAINT     �   ALTER TABLE ONLY public.pagos
    ADD CONSTRAINT creditos_pago FOREIGN KEY (idcredito) REFERENCES public.credito(idcredito) ON DELETE CASCADE;
 =   ALTER TABLE ONLY public.pagos DROP CONSTRAINT creditos_pago;
       public          postgres    false    204    2734    208            �
           2606    24736    ingresos idpersona    FK CONSTRAINT     �   ALTER TABLE ONLY public.ingresos
    ADD CONSTRAINT idpersona FOREIGN KEY (idpersona) REFERENCES public.persona(idpersona) ON DELETE CASCADE;
 <   ALTER TABLE ONLY public.ingresos DROP CONSTRAINT idpersona;
       public          postgres    false    210    2740    206            �
           2606    24741 !   certificados persona_certificados    FK CONSTRAINT     �   ALTER TABLE ONLY public.certificados
    ADD CONSTRAINT persona_certificados FOREIGN KEY (idpersona) REFERENCES public.persona(idpersona) ON DELETE CASCADE;
 K   ALTER TABLE ONLY public.certificados DROP CONSTRAINT persona_certificados;
       public          postgres    false    202    2740    210            �
           2606    24746    credito persona_credito    FK CONSTRAINT     �   ALTER TABLE ONLY public.credito
    ADD CONSTRAINT persona_credito FOREIGN KEY (idpersona) REFERENCES public.persona(idpersona) ON DELETE CASCADE;
 A   ALTER TABLE ONLY public.credito DROP CONSTRAINT persona_credito;
       public          postgres    false    210    2740    204            9   �   x���1�0���_��%�PGur'7� �8��р6�I��%/�q��y��]l����{ϞYT9t��ɒ�|��ϖ��S�_m~Nn�|A��$��ڝL ����V���B($N>�f>ڝLlڛ@�Xo�&'mڋ���I�,��N&�V�%�1�2�i#!�ޚ�!E��ր�uk�IL	�;"z ^���      ;   [   x�3�4202�5��50�400�3�,H�K�L�+I�A吘�\�ȺL��2D�1͸���JN�KN�IL�G��̄��k�E�!6]1z\\\ �-�      =   �   x�}���0�s�� ���1ނx�B�	�Q�?��0��4m	����n�sx`�`��@FT&p!��},X�Pf�����ߝf�ž	��IHL�8�׊G�%*�P��!Ya�@2t:�6Б�B,�"$MX#m!-��'��a�N�2�:{��K�xY�0/=��R�gH�e+)�gh�A����C��� �셊      ?   �  x�u�An�0��}�)R�%��m�tsԝ$��?�ZC��}�$�%��d�Hݤ޿��/�o��������k�g6'�vj3l?�2[I���)��|j���Z��S[`˩5X;�{��n+�c�����J�E=H����-�A�}%�"7���En%-������X�/׏����e)kɳ��_0�aF`J4���`���(L�)0�`:���L��0F`���l��᜙�᜙�M�,O�
�}���<� /�0��y�� /������y����a*���h�;��0ا&��0F`Fa��T�i�Q+�����S+5XܡZƊ�Om��`1���
�O�X��b�i+:X�>�,f_}���7��o���^>��g�n���f�0Y��ٶ4v�a��m�%X�cG[`e�`��]���ûi�7�,lmG��O����+����ٶپn�� ���S      A   �   x�M��N�0��� ��\i!B9P'.g�XJ��&���Qr]��7��R
g��&�'�8Q��F?�Zx��)3�؍�P���1���ا+��r��v�a�QV[��0��� 5�,���pZ)g���1�i�ԧi(>�3�.tY�̂t�TF*Ym2��-e�����[Be`Fކ2���LaE�S�T��?�� ��i(�יK����q���S٭}�1�~  {f�      B      x�3�L/*-�7�44261����� 64T     