/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rmiServer;

import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

/**
 *
 * @author Javier
 */
public class Server {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        try {
            // Creamos un objeto de la clase que implementa la interface
            ImplemntInterface obj = new ImplemntInterface();
            //  Exporta el objeto de la clase que implementa la interface
            //  antes construido. Lo estamos enviando al stub (stub: Stub: es un
            //  objeto que encapsula el método que deseamos invocar remotamente. 
            // Así el llamado remoto es semejante a un llamado local.)
            RemoteInterface stub = (RemoteInterface) UnicastRemoteObject.exportObject(obj,0);
            // Enlazamos el objeto remoto (stub) al registro.
            Registry registry = LocateRegistry.createRegistry(8888);
            registry.bind("RMI", stub);
            System.out.println("Server ready");
        } catch (AlreadyBoundException | RemoteException e) {
            System.out.println("Server exception: " + e.toString());
        }
    }
    
}
