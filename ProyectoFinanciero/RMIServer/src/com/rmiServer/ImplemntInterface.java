/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rmiServer;

//import java.sql.Connection;
//import java.sql.DriverManager;
import com.conexion.Conexion;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author gugle
 */
// Implementing the remote interface
public class ImplemntInterface implements RemoteInterface {
    //Connection connection;
    ArrayList results = null;

    public ImplemntInterface() {

    }

    // Implementing the interface method
    @Override
    public int countCliente() {
        String cadena = "";
        int count = 0;
        ResultSet resultSet;
        try {
            Conexion conexion = new Conexion();
            //cadena = "SELECT COUNT(*) AS COUNT FROM `persona`";
            cadena="SELECT COUNT(*) AS count FROM persona";
            if (conexion.getMessage().equals("ok")) {
                resultSet = conexion.select(cadena);
                if (resultSet.next()) {
                    //count = Integer.parseInt(resultSet.getString("COUNT")) + 1;
                    count = Integer.parseInt(resultSet.getString("count")) + 1;
                } else {
                    count = 0;
                }
                conexion.close();
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return count;
    }

    @Override
    public String insertarCliente(int idCliente, String nombre, String apellido, String genero, String direccion, String cedula) {
        String message = "", cadena = "";
        try {
            Conexion conexion = new Conexion();
            //cadena = "INSERT INTO `persona`(`idPersona`, `Nombre`, `Apellido`, `Genero`, `Dirección`, `CI`) VALUES (" + idCliente + ",'" + nombre + "','" + apellido + "','" + genero + "','" + direccion + "','" + cedula + "')";
            cadena = "INSERT INTO persona(idpersona, cedula, nombre, apellido, genero, direccion) VALUES (" + idCliente + ",'" + cedula + "','" + nombre + "','" + apellido + "','" + genero + "','" + direccion + "')";
            if (conexion.getMessage().equals("ok")) {
                if (conexion.update(cadena) > 0) {
                    message = "Cliente registrado";
                } else {
                    message = "Error al momento de registrar el cliente";
                }
                conexion.close();
            } else {
                message = "Contáctese con el administrador del sistema";
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return message;
    }

    @Override
    public int countRolPagos() {
        String cadena = "";
        int count = 0;
        ResultSet resultSet;
        try {
            Conexion conexion = new Conexion();
            //cadena = "SELECT COUNT(*) AS COUNT FROM `ingresos`";
            cadena = "SELECT COUNT(*) AS count FROM ingresos";
            if (conexion.getMessage().equals("ok")) {
                resultSet = conexion.select(cadena);
                if (resultSet.next()) {
                    //count = Integer.parseInt(resultSet.getString("COUNT")) + 1;
                    count = Integer.parseInt(resultSet.getString("count")) + 1;
                } else {
                    count = 0;
                }
                conexion.close();
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return count;
    }
    
    @Override
    public String insertarRolPagos(int idIngreso, String rolPagos, String fecha, int idCliente, double ingreso) {
        String message = "", cadena = "";
        try {
            Conexion conexion = new Conexion();
            //cadena = "INSERT INTO `ingresos`(`idIngresos`, `Rolpagos`, `Fecha`, `IdPersona`, `Ingresos`) VALUES ("+idIngreso+",'"+rolPagos+"','"+fecha+"',"+idCliente+","+ingreso+")";
            cadena = "INSERT INTO ingresos(idingresos, rolpagos, fecha, idpersona, ingresos) VALUES ("+idIngreso+",'"+rolPagos+"','"+fecha+"',"+idCliente+","+ingreso+")";
            if (conexion.getMessage().equals("ok")) {
                if (conexion.update(cadena) > 0) {
                    message = "Rol de pagos registrado";
                } else {
                    message = "Error al momento de registrar el rol de pagos";
                }
                conexion.close();
            } else {
                message = "Contáctese con el administrador del sistema";
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return message;
    }
    
    @Override
    public int countCertificado() {
        String cadena = "";
        int count = 0;
        ResultSet resultSet;
        try {
            Conexion conexion = new Conexion();
            //cadena = "SELECT COUNT(*) AS COUNT FROM `certificados`";
            cadena = "SELECT COUNT(*) AS count FROM certificados";
            if (conexion.getMessage().equals("ok")) {
                resultSet = conexion.select(cadena);
                if (resultSet.next()) {
                    //count = Integer.parseInt(resultSet.getString("COUNT")) + 1;
                    count = Integer.parseInt(resultSet.getString("count")) + 1;
                } else {
                    count = 0;
                }
                conexion.close();
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return count;
    }
    
    @Override
    public String insertarCertificado(int idCertificado, String certificadoVotacion, String prediosArrendamiento, String fecha, int idCliente, String impuesto, String servicioBasico) {
        String message = "", cadena = "";
        try {
            Conexion conexion = new Conexion();
            //cadena="INSERT INTO `certificados`(`idcertificados`, `votacion`, `Prediosoarrendamiento`, `fecha`, `idPersona`, `Ivaorenta`, `Serviciosbasicos`) VALUES ("+idCertificado+",'"+certificadoVotacion+"','"+prediosArrendamiento+"','"+fecha+"',"+idCliente+",'"+impuesto+"','"+servicioBasico+"')";
            cadena="INSERT INTO certificados(idcertificados, votacion, prediosoarrendamiento, fecha, idpersona, ivaorenta, serviciosbasicos) VALUES ("+idCertificado+",'"+certificadoVotacion+"','"+prediosArrendamiento+"','"+fecha+"',"+idCliente+",'"+impuesto+"','"+servicioBasico+"')";
            if (conexion.getMessage().equals("ok")) {
                if (conexion.update(cadena) > 0) {
                    message = "Certificados registrados";
                } else {
                    message = "Error al momento de registrar los certificados";
                }
                conexion.close();
            } else {
                message = "Contáctese con el administrador del sistema";
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return message;
    }
    
    @Override
    public ArrayList buscarCliente(String cedula)
    {
        Conexion conexion = new Conexion();
        ResultSet resultSet;
        //String cadena = "SELECT `idPersona` AS idCliente, `Nombre`, `Apellido`, `Genero`, `Dirección` AS direccion, `CI` FROM `persona` WHERE CI='"+cedula+"'";
        String cadena = "SELECT idpersona AS idCliente, nombre, apellido, genero, direccion, cedula FROM persona WHERE cedula='"+cedula+"'";
        if (conexion.getMessage().equals("ok")){
            try{
                resultSet = conexion.select(cadena);
                results = new ArrayList();

                if (resultSet.next()) {
                    results.add(resultSet.getString("idCliente"));
                    results.add(resultSet.getString("nombre"));
                    results.add(resultSet.getString("apellido"));
                    results.add(resultSet.getString("genero"));
                    results.add(resultSet.getString("direccion"));
                }
            }
            catch (SQLException exp)
            {
                System.out.println("Error: " + exp);
            }
        }
        return results;
    }
    
}
