/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.conexion;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 *
 * @author Javier
 */
public class Conexion {
    
    
    public Connection connection;
    private String message;
    private Statement statement;
    ResultSet resultSet;

    public Conexion() {

        try {
            //Class.forName("com.mysql.jdbc.Driver");
            //connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/proyectofinanciero", "root", "");
            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/Finan", "postgres", "12345");
            statement = connection.createStatement
            (ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
            message="ok";
        } catch (Exception ex) {
            message=ex.getMessage();
        }
    }
    
    public ResultSet select(String sentence){        
        
        if(message.equals("ok")){
            try{
                resultSet = statement.executeQuery(sentence);
            }
            catch(Exception ex){
                message=ex.getMessage();
            }
        }
        return resultSet;
    }

    public int update(String sentence){        
        int numReg = -1;
        if(message.equals("ok")){
            try{
                numReg = statement.executeUpdate(sentence);
            }
            catch(Exception ex){
                message=ex.getMessage();
            }
        }
        return numReg;
    }
    
     public void close(){
        try{
            resultSet.close();
            statement.close();
            connection.close();
        }
        catch(Exception ex){
            message = ex.getMessage();
        }
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
    
     
    
}
